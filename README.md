# document-processing
Correct the perspective of a document for its further processing.

## Usage
Example made in Python 3.6.

### Installing dependencies

Run the commands below in your terminal (*make sure you have python3 installed*):

#### Easy mode
```
pip3 install -r requirements.txt
```

#### Run test
*In this example we already have two images called `front.png` and `back.png`.*

1. Get a picture of the front and back face of a document (must be a paraguayan cedula). Consider placing the document on a dark background (preferably dark green, dark blue or black) and save *your picture* in the root folder.

2. Run the script with the following command:
```
python3 testcorrection.py your_picture_front.extension your_picture_back.extension --MODEL model/frozen_inference_graph.pb --LABELS data/labelmap.pbtxt
```
*Pro tip: Add --save to your command to save the results (`python3 testcorrection.py front.png back.png --MODEL model/frozen_inference_graph.pb --LABELS data/labelmap.pbtxt --save`). You will get four images, two results for each face.*

3. Voilà!.

