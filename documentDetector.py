# import
import os
import cv2
import sys
import numpy as np
import tensorflow as tf

from utils import label_map_util
from utils import visualization_utils as vis_util


class DocumentDetector:
	def __init__(self):
		# -> alloc memory exceeds 10% <-
		os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

		self.NUM_CLASSES = 1
		MODEL = 'model/frozen_inference_graph.pb'
		LABELS = 'data/labelmap.pbtxt'

		# label map
		label_map = label_map_util.load_labelmap(LABELS)
		categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=self.NUM_CLASSES, use_display_name=True)
		category_index = label_map_util.create_category_index(categories)

		# load the tf model into memory
		detection_graph = tf.Graph()
		with detection_graph.as_default():
			od_graph_def = tf.GraphDef()
			with tf.gfile.GFile(MODEL, 'rb') as fid:
				serialized_graph = fid.read()
				od_graph_def.ParseFromString(serialized_graph)
				tf.import_graph_def(od_graph_def, name='')

			# -> config=alloc memory exceeds 10% <-
			sess = tf.Session(graph=detection_graph, config=tf.ConfigProto(inter_op_parallelism_threads=1, intra_op_parallelism_threads=1))

		self.category_index = category_index
		self.detection_graph = detection_graph
		self.sess = sess

	def perform_detection(self, image, image_expanded):
		''' Run the detector.

			Receives the source image and the expanded image.
			Returns the detected and cropped image.
		'''
		copy = image.copy()

		# input tensor
		image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
		# output tensors
		detection_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')

		# detect
		detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
		detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')

		# get objects detected
		num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')

		# running the model
		(boxes, scores, classes, num) = self.sess.run(
			[detection_boxes, detection_scores, detection_classes, num_detections],
			feed_dict={image_tensor: image_expanded})

		# visualise the results
		roi, array_coord = vis_util.visualize_boxes_and_labels_on_image_array(
			image,
			np.squeeze(boxes),
			np.squeeze(classes).astype(np.int32),
			np.squeeze(scores),
			self.category_index,
			use_normalized_coordinates=True,
			line_thickness=3,
			min_score_thresh=0.60)

		ymin, xmin, ymax, xmax = array_coord

		shape = np.shape(roi)
		im_width, im_height = shape[1], shape[0]
		(left, right, top, bottom) = (xmin*im_width, xmax*im_width, ymin*im_height, ymax*im_height)

		# crop
		top = int(top)
		right = int(right)
		bottom = int(bottom)
		left = int(left)
		image_cropped = copy[top:bottom, left:right]
		# gray
		gray_image = cv2.cvtColor(image_cropped, cv2.COLOR_BGR2GRAY)

		return roi, image_cropped, gray_image

	def detection_handler(self, images):
		''' Function to handle detection.

			Receives images.
			Returns aligned and cropped images.
		'''
		try:
			i = 0
			results = []
			originals = []
			gray_images = []
			image_roi = []

			for image in images:
				image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
				original = image.copy()
				image_expanded = np.expand_dims(image, axis=0)
				roi, image_cropped, gray_image = self.perform_detection(image, image_expanded)
				originals.append(original)
				results.append(image_cropped)
				gray_images.append(gray_image)
				image_roi.append(roi)
				i += 1

			return results, originals, gray_images, image_roi

		except:
			return False
