### unused

import os
from dotenv import load_dotenv


class Config:
	def __init__(self):
		return

	def aws_config(self):
		load_dotenv()
		# AWS credentials
		AWS_ACCESS_KEY_ID = os.getenv('aws_access_key_id')
		AWS_SECRET_ACCESS_KEY = os.getenv('aws_secret_access_key')
		REGION_NAME = os.getenv('region_name')

		# S3 Config
		AWS_S3_URL = os.getenv('AWS_S3_URL')
		AWS_S3_BUCKET_NAME = os.getenv('AWS_S3_BUCKET_NAME')

		return AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, REGION_NAME, AWS_S3_URL, AWS_S3_BUCKET_NAME
