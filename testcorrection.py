import cv2
import argparse
from documentDetector import DocumentDetector


if __name__ == '__main__':
	service = DocumentDetector()

	# flags
	parser = argparse.ArgumentParser()
	parser.add_argument('front', default='front.png', metavar='N', type=str, help='Front image')
	parser.add_argument('back', default='back.png', metavar='N', type=str, help='Back image')
	parser.add_argument('-m', '--MODEL', required=True, help='Tensorflow Model to run detector.')
	parser.add_argument('-l', '--LABELS', required=True, help='Detector labels.')
	parser.add_argument('--save', required=False, action='store_true', help='Allows saving the results.')
	args = parser.parse_args()
	save = args.save

	# get test images
	test_image_f = 'test/'+args.front
	test_image_b = 'test/'+args.back
	#test_reference_f = 'src/front.png'
	#test_reference_b = 'src/back.png'
	# read images
	test_image_f = cv2.imread(test_image_f, cv2.IMREAD_COLOR)
	test_image_b = cv2.imread(test_image_b, cv2.IMREAD_COLOR)
	# build dicts
	images = [test_image_f, test_image_b]
	#references = [test_reference_f, test_reference_b]

	# send images and get results
	results, _, gray, roi = service.detectionHandler(images, args.MODEL, args.LABELS)

	if results is not False:
		i = 0
		for result in results:
			# show results
			cv2.imshow('ROI', roi[i])
			cv2.imshow('Gray', gray[i])
			cv2.imshow('Result', result)
			cv2.waitKey(0)
			if save:
				cv2.imwrite(f'result_00{i}.png', result)
				cv2.imwrite(f'result_00{i}_gray.png', gray[i])
				cv2.imwrite(f'result_00{i}_roi.png', roi[i])
			i += 1
