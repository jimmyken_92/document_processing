from flask import Flask
from processing import processing
from responseHandler import AfterResponse
# --------------------------------------- #
import os
import cv2
import sys
import yaml
import requests
import unittest
from timeit import time
from pathlib import Path
# --------------------------------------- #


directory = 'unittest/'


class TestApp(unittest.TestCase):
	def test_app(self):
		with open('env.yaml') as data:
			env = yaml.load(data)

		APP_HOST = 'localhost'
		APP_ROUTE = env['APP']['ROUTE']
		APP_PORT = env['APP']['PORT']
		app = Flask(__name__)

		with app.app_context():
			app.register_blueprint(processing, url_prefix=APP_ROUTE)
			AfterResponse(app)

		# if __name__ == '__main__':
		# 	app.run(APP_HOST, debug=False, port=APP_PORT)

		print(f'Start unittest: {APP_ROUTE}')

		# set the source path
		images = ['front.png', 'back.png']
		# set data
		url = f'http://{APP_HOST}:{APP_PORT}{APP_ROUTE}'
		Path(directory).mkdir(parents=True, exist_ok=True)

		# files
		files = {
			'data/front': None,
			'data/back': None,
			'title': 'test_images'
		}

		# test image reading
		img_face = 0
		for image in images:
			opencvimage = cv2.imread(f'test/{image}')
			cv2.imwrite(f'{directory}readed_{image}', opencvimage)

			if img_face == 0:
				files['data/front'] = image
			elif img_face > 0:
				files['data/back'] = image

			img_face += 1

		try:
			print(f'{url} - Connection started')
			response = app.post(url)
			self.assertEqual(response.status_code, 200)
		except:
			print("Connection refused")


if __name__ == '__main__':
	unittest.main()
