#!/bin/sh

#
# Script for deploy
#
usage="$(basename "$0") <command> [-h] -- Script for build, tag, push and deploy microservice to AWS ECR

commands:
    build <NPM_TOKEN>       Build the docker file
    tag <SERVICE_NAME>      Tag the docker
    push <SERVICE_NAME>     Push the latest docker
    deploy <SERVICE_NAME>   deploy the lastest docker image on ecs

where:
    -h  show this help text"

_build() {
  {
    docker build -t your-document-processor-aws .
  } || {
    echo "On build error"
    exit 1
  }
}

_tag() {
  {
    docker tag your-document-processor-aws:latest 000000000000.dkr.ecr.us-east-1.amazonaws.com/your-document-processor-aws:latest
  } || {
    echo "On tag error"
    exit 1
  }
}

_push() {
  {
    docker push 000000000000.dkr.ecr.us-east-1.amazonaws.com/your-document-processor-aws:latest
  } || {
    echo "On push error"
    exit 1
  }
}

_deploy() {
  {
    aws ecs update-service  --force-new-deployment --cluster "devstack-ECSCluster-0000Q0Q0QQ0Q" --service your-document-processor-aws
  } || {
    echo "On deploy error"
    exit 1
  }
}

while getopts ':h:' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
  esac
done

case "$1" in
    build   ) _build
              exit
              ;;
    tag     ) _tag
              exit
              ;;
    push    ) _push
              exit
              ;;
    deploy  ) _deploy
              exit
              ;;
    *       ) _build &&
              _tag &&
              _push &&
              _deploy
esac
