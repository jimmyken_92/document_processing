from enum import Enum


class Errors(Enum):
	error422 = { 'httpStatus': 422, 'errorCode': 'invalid.image.file', 'message': 'Invalid image extension' }
	error500 = { 'httpStatus': 500, 'errorCode': 'internal.server.error', 'message': 'Internal server error' }
