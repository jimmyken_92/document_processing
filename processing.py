# imports
import os
import cv2
import json
import jsonpickle
import numpy as np
from PIL import Image
from flask import Blueprint, request, Response, current_app as app

# import image detector
from documentDetector import DocumentDetector

# s3
import io
import boto3
#import logging
from botocore.exceptions import ClientError

# config
#from config import Config
import yaml
# errors
from errors import Errors


processing = Blueprint('processing', __name__)

service = DocumentDetector()
error422 = Errors.error422.value

with open('env.yaml') as data:
	env = yaml.load(data)

AWS_ACCESS_KEY_ID = env['AWS']['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = env['AWS']['AWS_SECRET_ACCESS_KEY']
REGION_NAME = env['AWS']['REGION_NAME']
AWS_S3_URL = env['AWS']['AWS_S3_URL']
AWS_S3_BUCKET_NAME = env['AWS']['AWS_S3_BUCKET_NAME']


@processing.route('/image/document/detection', methods=['POST'])
def document_detection():
	try:
		image_file_f = request.files['data/front']
		image_file_b = request.files['data/back']
		received_image_name = request.form['title']

	except Exception as e:
		return e

	print(received_image_name)

	arr_f = np.asarray(bytearray((image_file_f).read()), dtype='uint8')
	image_f = cv2.imdecode(arr_f, cv2.IMREAD_COLOR)
	arr_b = np.asarray(bytearray(image_file_b.read()), dtype='uint8')
	image_b = cv2.imdecode(arr_b, cv2.IMREAD_COLOR)
	images = [image_f, image_b]

	# start
	results, originals, gray_images, _ = service.detection_handler(images)

	if results is not False:
		''' Upload a file to an S3 bucket

			:param file_name: File to upload
			:param bucket: Bucket to upload to
			:param object_name: S3 object name
		'''
		# upload gray images
		rekognition = boto3.client('rekognition',
			aws_access_key_id=AWS_ACCESS_KEY_ID,
			aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
			region_name=REGION_NAME
		)

		# set rekognition_response dictionary
		rekognition_response = {
			'front': {},
			'rear': {}
		}
		imageface = 0
		for r_image in results:
			print('Start OCR')

			image_array = Image.fromarray(r_image)
			stream = io.BytesIO()
			image_array.save(stream, format='JPEG')
			binary = stream.getvalue()

			if imageface == 0:
				rekognition_response['front'] = rekognition.detect_text(
					Image={ 'Bytes': binary }
				)
			else:
				rekognition_response['rear'] = rekognition.detect_text(
					Image={ 'Bytes': binary }
				)

			imageface += 1

		# upload images
		@app.after_response
		def post_processing():
			print('Post Processing begins')

			# open boto3 client
			client = boto3.client('s3',
				aws_access_key_id=AWS_ACCESS_KEY_ID,
				aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
				region_name=REGION_NAME
			)

			# upload results
			i = 0
			for result in results:
				# image name
				if i == 0:
					image_name = f'document-front/{received_image_name}'
				else:
					image_name = f'document-back/{received_image_name}'

				# set dicts
				files = []
				keys = []

				# gray image
				gray_arr = Image.fromarray(gray_images[i])
				gray_stream = io.BytesIO()
				gray_arr.save(gray_stream, format='JPEG')
				gray_stream.seek(0)
				files.append(gray_stream)
				gray_key = f'{image_name}_gray'
				keys.append(gray_key)
				# cropped image
				arr = Image.fromarray(result)
				stream = io.BytesIO()
				arr.save(stream, format='JPEG')
				stream.seek(0)
				files.append(stream)
				key = f'{image_name}_cropped'
				keys.append(key)
				# original
				original_arr = Image.fromarray(originals[i])
				original_stream = io.BytesIO()
				original_arr.save(original_stream, format='JPEG')
				original_stream.seek(0)
				files.append(original_stream)
				original_key = image_name
				keys.append(original_key)

				# upload
				try:
					j = 0
					for file in files:
						got_r = client.upload_fileobj(file, AWS_S3_BUCKET_NAME, keys[j])
						j += 1
				except ClientError as e:
					print(e)
				i += 1
			print('Post Processing finished')

		# encode response using jsonpickle
		response_pickled = jsonpickle.encode(rekognition_response)
		return Response(response=response_pickled, status=200, mimetype='application/json')

	else:
		error_pickled = jsonpickle.encode(error422)
		return Response(response=error_pickled, status=422, mimetype='application/json')


@processing.route('/healthcheck', methods=['GET', 'POST'])
def healthcheck():
	try:
		# setup response
		response = { 'status': 200 }

		# encode response using jsonpickle
		response_pickled = jsonpickle.encode(response)
		return Response(response=response_pickled, status=200, mimetype='application/json')

	except:
		error_pickled = jsonpickle.encode(error422)
		return Response(response=error_pickled, status=422, mimetype='application/json')
