# imports
import yaml
from flask import Flask
from processing import processing
from responseHandler import AfterResponse


with open('env.yaml') as data:
	env = yaml.load(data)

APP_HOST = env['APP']['HOST']
APP_ROUTE = env['APP']['ROUTE']
APP_PORT = env['APP']['PORT']
app = Flask(__name__)

print(APP_ROUTE)

with app.app_context():
	app.register_blueprint(processing, url_prefix=APP_ROUTE)
	AfterResponse(app)


if __name__ == '__main__':
	app.run(APP_HOST, debug=False, port=APP_PORT)
