FROM python:3.7-slim

WORKDIR /usr/src/app
ADD . /usr/src/app

RUN apt-get update
RUN apt-get install libsm6 libxext6 libxrender-dev libglib2.0-0 -y
RUN apt-get install libgl1-mesa-glx -y
RUN pip install -r requirements.txt
RUN export FLASK_APP=app
RUN export FLASK_RUN_PORT=8095
RUN export FLASK_ENV=production
RUN export FLASK_DEBUG=1

EXPOSE 8095
COPY . .

CMD [ "python", "app.py" ]
